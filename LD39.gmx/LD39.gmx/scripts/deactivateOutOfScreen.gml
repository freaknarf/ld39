inst=argument0
tolerance=argument1
if object_exists(inst){
instance_deactivate_object(inst);
instance_activate_region(view_xview[0]-tolerance, view_yview[0]-tolerance,  view_wview[0]+2*tolerance, view_hview[0]+2*tolerance, true);
}
