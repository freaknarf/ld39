///CREATION OF OPTION
opt=ds_map_find_value(powerupsList,"option")

for (i=1; i<=opt; i+=1){
    if (ds_list_size(optionList)<opt){
        option=instance_create(round(x),round(y),oOption)
        with option {
            optionNum=other.opt
            owner=other.id
            image_blend=other.image_blend
            }
        ds_list_add(optionList,option)
    }
};
