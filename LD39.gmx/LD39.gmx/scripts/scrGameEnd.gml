/// CREATE GAME OVER STATE : 
//PUT THIS IN THE PLAYER EXPLOSION ANIMATION END.
if credits<=0 and instance_number(oPlayer)=0{
    room_goto(rDeath)
    credits=3
    if multiplayer
    credits=5
    continues--
    instance_destroy()
}
