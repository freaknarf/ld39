
player=instance_nearest(x,y,oPlayer)
if instance_exists(player){
    bulletShot=instance_create(x+sprite_width/2,y,oBulletEnemy);
    //time to reload
    canShoot=false;
    alarm[0]=reloadTime;
       
    //sets bullet shot speed
    if (bulletShot){
        with bulletShot{move_towards_point(other.player.x,other.player.y,(3+level+difficulty)/2)}
    }
    if alarm[1]=-1
        alarm[1]=reloadTime
    shooting=false
}
