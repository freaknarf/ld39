///gamepad
device=argument0
player=argument1
//
if player=0{
up=(u==0)&&(gamepad_axis_value(device,gp_axislv)<-.4)
dp=(d==0)&&(gamepad_axis_value(device,gp_axislv)>.4)
lp=(l==0)&&(gamepad_axis_value(device,gp_axislh)<-.4)
rp=(r==0)&&(gamepad_axis_value(device,gp_axislh)>.4)
//AXIS
if gamepad_axis_value(device,gp_axislv)>.4
    d=1 else d=0
if gamepad_axis_value(device,gp_axislv)<-.4
    u=1 else u=0
if gamepad_axis_value(device,gp_axislh)<-.4
    l=1 else l=0
if gamepad_axis_value(device,gp_axislh)>.4
    r=1 else r=0
if gamepad_button_check(device,gp_face1)
    f=1 else f=0
if gamepad_button_check_pressed(device,gp_face1)
    fp=1 else fp=0
if gamepad_button_check(device,gp_face2)
    select=1 else select=0
if gamepad_button_check_pressed(device,gp_face2)
    selectp=1 else selectp=0
startBtn=gamepad_button_check_pressed(device,gp_start)
}
if player=1{
up2=(u==0)&&(gamepad_axis_value(device,gp_axislv)<-.4)
dp2=(d==0)&&(gamepad_axis_value(device,gp_axislv)>.4)
lp2=(l==0)&&(gamepad_axis_value(device,gp_axislh)<-.4)
rp2=(r==0)&&(gamepad_axis_value(device,gp_axislh)>.4)
//AXIS
if gamepad_axis_value(device,gp_axislv)>.4
    d2=1 else d2=0
if gamepad_axis_value(device,gp_axislv)<-.4
    u2=1 else u2=0
if gamepad_axis_value(device,gp_axislh)<-.4
    l2=1 else l2=0
if gamepad_axis_value(device,gp_axislh)>.4
    r2=1 else r2=0
if gamepad_button_check(device,gp_face1)
    f2=1 else f2=0
if gamepad_button_check_pressed(device,gp_face1)
    fp2=1 else fp2=0
if gamepad_button_check(device,gp_face2)
    select2=1 else select2=0
if gamepad_button_check_pressed(device,gp_face2)
    selectp2=1 else selectp2=0
startBtn2=gamepad_button_check_pressed(device,gp_start)
}
