player=argument0
mov=argument1

if instance_exists(player){

    switch state{
    case 0 : hspeed=-mov+oCamera.spdX break;
    
    case 1 : hspeed=mov+oCamera.spdX
             vspeed=-sign(y-oPlayer.y)*(mov) break;
    //reverse        
    case 2 : hspeed=1*(mov)+oCamera.spdX
             vspeed=0 break;
    }
}
