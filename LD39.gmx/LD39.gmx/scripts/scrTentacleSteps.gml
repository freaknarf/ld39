/// Update path positions
if instance_exists(oPlayer)
{
player=instance_nearest(x,y,oPlayer)

//scrTentaclePositions(10,p1,0,x,y,.02,.005)
scrTentaclePositions(5,p1,1,player.x,player.y,.02,.005)
scrTentaclePositions(10,p1,2,player.x,player.y,.02,.005)
scrTentaclePositions(20,p1,3,player.x,player.y,.02,.005)
scrTentaclePositions(30,p1,4,player.x,player.y,.02,.005)
///!\ bug html ?!

//scrTentaclePositions(80,p1,5,player.x,player.y,.02,.005)
//control tentacle spots
scrTentaclePositions(5,p2,1,player.x,player.y,.02,.005)
scrTentaclePositions(10,p2,2,player.x,player.y,.02,.005)
scrTentaclePositions(20,p2,3,player.x,player.y,.02,.005)
scrTentaclePositions(30,p2,4,player.x,player.y,.02,.005)

//scrTentaclePositions(80,p2,5,player.x,player.y,.02,.005)

//control tentacle spots
/*var sprite=argument5
var lifeweak=argument6
var lifestrong=argument7
var col1=argument8
var col2=argument9
var strongPos=argument10*/
enemyList1=controlTentacles(p1, sTentacleShooting, nTentacles, true,enemyList1,1,5,50,colorNES[18],colorNES[53],round(nTentacles)/2+1,sEnemyStaticBall);
enemyList2=controlTentacles(p2, sTentacleShooting, nTentacles, true,enemyList2,1,5,50,colorNES[18],colorNES[53],round(nTentacles)/2+1,sEnemyStaticBall);

//move
if !irandom(10)
    x+=sign(x-player.x)
if !irandom(10)
    y+=sign(y-player.y)
 //move tentacles
 
path_shift(p1, x-path_get_x(p1,0),-12+ y-path_get_y(p1, 0));
path_shift(p2, x-path_get_x(p2,0),+12+ y-path_get_y(p2, 0));
       
}

