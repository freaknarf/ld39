var vx=argument0
var vy=argument1
var val=0;
// Vertical
repeat(abs(vy)) {
    if (!place_meeting(x, y + sign(vy), oParSolid))
        {y += sign(vy); }
    else {
        vy = 0;
        val="vy"
        break;
    }
}

// Horizontal
repeat(abs(vx)) {

    if (!place_meeting(x + sign(vx), y, oParSolid))
        x += sign(vx); 
    else {
        vx = 0;
        val="vx"
        break;
    }
}
return val
