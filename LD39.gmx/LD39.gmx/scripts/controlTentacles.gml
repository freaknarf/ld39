///draw_path_sprite(path, sprite_index, sprite_count, rotate)

var pos, pos_next, xx, yy, xxx, yyy, angle;
var path = argument0;
var spriteStrong = argument1;
var spriteWeak = argument11;
var sprite_count = argument2;
var rotate = argument3;
var inc = 1/(sprite_count-1);
var enemyList=argument4;
var rot=argument5
var lifeweak=argument6
var lifestrong=argument7
var col1=argument8
var col2=argument9
var strongPos=argument10
if(rotate)
{
  for(pos=0; pos<1; pos += inc) {
    posInt=round((sprite_count)*pos)
    pos_next = min(pos+inc, 1);
    xx = path_get_x(path, pos);
    yy = path_get_y(path, pos);
    xxx = path_get_x(path, pos_next);
    yyy = path_get_y(path, pos_next);
    angle = point_direction(xx, yy, xxx, yyy);

    if (enemyList[posInt]=noone){    
      if round(posInt)=round(sprite_count-1){
           // enemyList[posInt].shooting=true
            }
    //crete instance and offset for tentcales moves
        enemyList[posInt]=instance_create(xx,yy,oTentacleSpot)
        if posInt=strongPos{
            enemyList[posInt].sprite_index=spriteStrong
            enemyList[posInt].life=lifestrong
            enemyList[posInt].image_blend=col1
            enemyList[posInt].shooting=true
            enemyList[posInt].depth-=1
            }else{
            enemyList[posInt].sprite_index=spriteWeak
            enemyList[posInt].life=lifeweak
            enemyList[posInt].image_blend=col2
            enemyList[posInt].shooting=false
            enemyList[posInt].alarm[1]=0
            }
  
        
        enemyList[posInt].t=posInt*(5)

    }
    else if instance_exists(enemyList[posInt]) {
    //moves tentacles parts in sin waves
    
    enemyList[posInt].t++
    enemyList[posInt].x=xx +(posInt)*point_distance(enemyList[posInt].x,enemyList[posInt].y,oPlayer.x,oPlayer.y)/80*sin(degtorad(enemyList[posInt].t))
    enemyList[posInt].y=yy +(posInt)*point_distance(enemyList[posInt].x,enemyList[posInt].y,oPlayer.x,oPlayer.y)/80*cos(degtorad(enemyList[posInt].t))
    enemyList[posInt].image_angle=22.5*(angle div 22.5)
    if enemyList[posInt].life=1
    enemyList=scrDestroyEnemyList(enemyList,sprite_count);
    
       
    }
  }
}
else {
  for(pos=0; pos<1; pos += inc) {
    xx = path_get_x(path, pos);
    yy = path_get_y(path, pos);
        if !instance_exists(enemyList[posInt]){
        enemyList[posInt]=instance_create(xx,yy,oTentacleSpot)
}
    else{
    enemyList[posInt].t++
    enemyList[posInt].x=xx +50*sin(degtorad(enemyList[posInt].t))
    enemyList[posInt].y=yy + 50*cos(degtorad(enemyList[posInt].t))   
    }
 }
  
}
return enemyList;
