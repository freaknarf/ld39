r=argument0
path=argument1
pathPoint=argument2
x0=path_get_point_x(path, pathPoint-1)
y0=path_get_point_y(path, pathPoint-1)
xTarget=argument3
yTarget=argument4
spd=argument5
spdPath=argument6

xx=x0+r*sin(degtorad(-90+point_direction(xTarget,yTarget,x0,y0)))
yy=y0+r*cos(degtorad(-90+point_direction(xTarget,yTarget,x0,y0)))
path_change_point(path, pathPoint, lerp(path_get_point_x(path, pathPoint),xx,spd), lerp(path_get_point_y(path, pathPoint),yy,spd), spdPath);
