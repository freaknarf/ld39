//CHECK CONTROLS

var u = argument0
var d = argument1

//MOVE WINGS
if (d){
    if round(image_index)>0
        image_speed=-.2
    else {
        image_speed=0
        image_index=0
    }
}
        
if (u){
    if image_index<=image_number-1
        image_speed=.2
    else{
        image_speed=0
        image_index=image_number-1
        }
}
///STABILIZE        
if (!d and !u){
    if round(image_index)!=image_number/2{
        if round(image_index)>2
            image_speed=-.05
        else
        if round(image_index)<2
            image_speed=.05
        else
            image_speed=0
    }
}
//draw sprite
draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha)
