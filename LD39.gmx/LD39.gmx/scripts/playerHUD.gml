var player=argument0
var xx=argument1
var yy=argument2
var height=argument3
if instance_exists(player){
    offset=view_wview[0]/7
    for (i=0; i<7; i+=1){

        colorRect=c_dkgray
        draw_rectangle_color(xx+i*offset,yy,xx+view_wview[0]+i*offset,yy,colorRect,colorRect,colorRect,colorRect,1)
        colorRect=c_green
        if i=player.powState
        draw_rectangle_color(xx+i*offset,yy,xx+i*offset+offset*player.pow*.01,yy+height,colorRect,colorRect,colorRect,colorRect,0)
    };
    map=player.powerupsList
    size = ds_map_size(map);
    key = ds_map_find_last(map)
    draw_set_halign(fa_left)
    draw_set_valign(fa_middle)
    draw_text(xx+2,2+yy,"SPEED")
    draw_text(xx+2+offset,2+yy,"MISSILE")
    draw_text(xx+2+offset*2,2+yy,"DOUBLE")
    draw_text(xx+2+offset*3,2+yy,"LASER")
    draw_text(xx+2+offset*4,2+yy,"OPTION")
    draw_text(xx+2+offset*5,2+yy,"?")
    draw_text(xx+2+offset*6,2+yy,"!")
    //score
    draw_text(xx+view_wview[0]/2,view_yview[0]+view_hview[0]-12,"SCORE : "+string(score)) 
}
