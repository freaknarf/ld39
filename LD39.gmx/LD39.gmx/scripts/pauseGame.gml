if pause = true
{
instance_deactivate_object(oPlayer)
instance_deactivate_object(oEnemy)
instance_deactivate_object(oBullets)
}
if pause = false
{
instance_activate_object(oPlayer)
instance_activate_object(oEnemy)
instance_activate_object(oBullets)
}

