///GUN SHOT SCRIPT
/*
IT'S A BIT HACKY : 
//////////////////////////////////////////////////////////////
//object_index : the bullet type (ex : oBulletPlayer)
bulletType=>argument0
// the player's ds list according to bullet type
bulletList=>argument1
//the max bullet ont the screen
maxBulletOnScreen=>argument2
//ability to shoot what
canShootBullets=>argument3
//what was the last bullet
bulletShot=>argument4
//what alarm triggers the ability to shoot (cooldown alarms)
alarm number =argument5
//bullet speeds
hspeed=>argument6 
vspeed=>argument7 
//reload time according to bullet
maxReloadBulletTime=>arg8
//fire buttons and button press event
ff=argument9
ffp=argument10
//////////////////////////////////////////////////////////////
*/

ff=argument9
ffp=argument10
var hsp=argument6
var vsp=argument7
var ret=true;
/// MAGIC BULLET
// IF FIRE PRESSED AND CAN SHOOT BULLETS
if autofire{
    var inst=0;
    if (ff and argument3){
    //IF N BULLETS ON SCREEN < MAX
        if ds_list_size(argument1)<argument2{
        //CREATE BULLET
        inst=instance_create(x+sprite_width/2,y,argument0);

        //BULLET OWNER IS SELF
        inst.owner=id
        inst.hspeed=hsp
        inst.vspeed=vsp 
        
        //ADD THIS ONE TO THE BULLET TYPE INSTANCE LIST
        ds_list_add(argument1,inst)
        //TRIGGER COOLDOWN ALARM
        alarm[argument5]=argument8;
        //CANNOT SHOOT
        ret=false;
        }
    }  
    else{
        //NO BULLET'S BEEN SHOT
       bulletShot=0
    }
    //ABLE TO SHOT WHEN BLASTING KEYBOARD
    if ((argument0=oBulletPlayer)
    and (ffp)) {
        b=instance_create(x+sprite_width/2,y,argument0);
        b.owner=id
        b.hspeed=10;//argument6
        b.vspeed=0//argument7
    }
}
else{
    if (ffp and argument3){
    //IF N BULLETS ON SCREEN < MAX
        if ds_list_size(argument1)<argument2{
        //CREATE BULLET
        inst=instance_create(x+sprite_width/2,y,argument0);
        //BULLET OWNER IS SELF
        inst.owner=id
        inst.hspeed=hsp
        inst.vspeed=vsp 
        //ADD THIS ONE TO THE BULLET TYPE INSTANCE LIST
        ds_list_add(argument1,argument4)
        //TRIGGER COOLDOWN ALARM
        alarm[argument5]=argument8;
        //CANNOT SHOOT
        ret=false;
        }
    }
}
//sets bullet shot speed

//return if can shot.
return ret
