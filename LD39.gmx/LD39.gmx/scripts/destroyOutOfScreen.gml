inst=argument0
tolerance=argument1
if instance_exists(inst)
//if inst.isScreenLocked=true
{
if inst.x<view_xview[0]-tolerance
or inst.y<view_yview[0]-tolerance
or inst.x>view_xview[0]+view_wview[0]+tolerance
or inst.y>view_yview[0]+view_hview[0]+tolerance
with inst instance_destroy();
}
