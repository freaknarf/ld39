inst=argument0
tolerance=argument1
if instance_exists(inst){
        instance_deactivate_object(inst);
        instance_activate_region(view_xview[0]-tolerance, view_yview[0]-tolerance,  view_wview[0]+3*tolerance, view_hview[0]+2*tolerance, true);
        instance_activate_region(view_xview[1]-tolerance, view_yview[1]-tolerance,  view_wview[1]+3*tolerance, view_hview[1]+2*tolerance, true);
        instance_activate_region(view_xview-tolerance, view_yview-tolerance,  view_wview+2*tolerance, view_hview+2*tolerance, true);

}
