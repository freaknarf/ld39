colour=argument0
thickness=argument1
alpha=argument2
for (i=0; i<=thickness; i+=1)
{
draw_sprite_ext(sprite_index,image_index,x+i,y,image_xscale,image_yscale,image_angle,colour,alpha)
draw_sprite_ext(sprite_index,image_index,x-i,y,image_xscale,image_yscale,image_angle,colour,alpha)
draw_sprite_ext(sprite_index,image_index,x,y+i,image_xscale,image_yscale,image_angle,colour,alpha)
draw_sprite_ext(sprite_index,image_index,x,y-i,image_xscale,image_yscale,image_angle,colour,alpha)
};


