////Display messages
var centerX=(view_xview[0]+view_wview[0])/2
var centerY=(view_yview[0]+view_hview[0])/2
if room=rGameOver
    draw_text(centerX,centerY,"GAME#OVER")
if pause
    draw_text(centerX,centerY,"PAUSE")
if room=rGameWin
    draw_text(centerX,centerY,"YOU#WON")
if room=rIntro
    draw_text(centerX,centerY,"PRESS#START")
if room=rDeath{
    draw_text(centerX,centerY,"CONTINUES LEFT :#"+string(continues))
}
