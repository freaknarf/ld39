if instance_exists(oPlayer){
    offset=view_wview[0]/7
    for (i=0; i<7; i+=1){
        colorRect=c_ltgray
        draw_rectangle_color(view_xview[0]+i*offset,view_yview[0]+view_hview[0],view_xview[0]+view_wview[0]+i*offset,view_yview[0]+view_hview[1],colorRect,colorRect,colorRect,colorRect,0)
        colorRect=c_dkgray
        draw_rectangle_color(view_xview[0]+i*offset,view_yview[0]+view_hview[0],view_xview[0]+view_wview[0]+i*offset,view_yview[0]+view_hview[1],colorRect,colorRect,colorRect,colorRect,1)
        colorRect=c_green
        if i=oPlayer.powState
        draw_rectangle_color(view_xview[0]+i*offset,view_yview[0]+view_hview[0],view_xview[0]+i*offset+offset*oPlayer.pow*.01,view_yview[0]+view_hview[1],colorRect,colorRect,colorRect,colorRect,0)
    };
    map=oPlayer.powerupsList
    size = ds_map_size(map);
    key = ds_map_find_last(map)
    draw_set_halign(fa_left)
    draw_set_valign(fa_middle)
    draw_text(view_xview[0]+2,2+view_yview[0]+view_hview[0],"SPEED")
    draw_text(view_xview[0]+2+offset,2+view_yview[0]+view_hview[0],"MISSILE")
    draw_text(view_xview[0]+2+offset*2,2+view_yview[0]+view_hview[0],"DOUBLE")
    draw_text(view_xview[0]+2+offset*3,2+view_yview[0]+view_hview[0],"LASER")
    draw_text(view_xview[0]+2+offset*4,2+view_yview[0]+view_hview[0],"OPTION")
    draw_text(view_xview[0]+2+offset*5,2+view_yview[0]+view_hview[0],"?")
    draw_text(view_xview[0]+2+offset*6,2+view_yview[0]+view_hview[0],"!")
    //score
    draw_text(view_xview[0]+view_wview[0]/2,view_hview[0]-12,"SCORE : "+string(score)) 
}

