///GUN SHOT SCRIPT

/*//obj index bullet type
bulletType=>argument0
//ex : oBulletPlayer
bulletList=>argument1
maxBulletOnScreen=>argument2
//ability to shoot what
canShootBullets=>argument3
bulletShot=>argument4
alarm number =argument5
hspeed=>argument6 
vspeed=>argument7 
maxReloadBulletTime=>arg8
*/

// IF FIRE PRESSED AND CAN SHOOT BULLETS
if autofire{
if (f and argument3){
//IF N BULLETS ON SCREEN < MAX
    if ds_list_size(argument1)<argument2{
    //CREATE BULLET
    argument4=instance_create(x+sprite_width/2,y,argument0);
    //BULLET OWNER IS SELF
    argument4.owner=id
    //ADD THIS ONE TO THE BULLET TYPE INSTANCE LIST
    ds_list_add(argument1,argument4)
    //TRIGGER COOLDOWN ALARM
    alarm[argument5]=argument8;
    //CANNOT SHOOT
    argument3=false;
    }
}  
else{
    //NO BULLET'S BEEN SHOT
   argument4=0
}
//ABLE TO SHOT WHEN BLASTING KEYBOARD
if ((argument0=oBulletPlayer)
and (fp)) {
    b=instance_create(x+sprite_width/2,y,argument0);
    b.owner=id
    b.hspeed=10;//argument6
    b.vspeed=0//argument7
}


}else{
if (fp and argument3){
//IF N BULLETS ON SCREEN < MAX
    if ds_list_size(argument1)<argument2{
    //CREATE BULLET
    argument4=instance_create(x+sprite_width/2,y,argument0);
    //BULLET OWNER IS SELF
    argument4.owner=id
    //ADD THIS ONE TO THE BULLET TYPE INSTANCE LIST
    ds_list_add(argument1,argument4)
    //TRIGGER COOLDOWN ALARM
    alarm[argument5]=argument8;
    //CANNOT SHOOT
    argument3=false;
    }
}
}
//sets bullet shot speed
if (argument4){
argument4.hspeed=argument6
argument4.vspeed=argument7
}
//return if can shot.
return argument3
